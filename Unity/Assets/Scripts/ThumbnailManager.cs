﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class ThumbnailManager : MonoBehaviour
{
    const int Thumbnails = 1000;
    const int VisibleLines = 50;
    const int AdjustFrequency = 30;

    public Transform container;
    public GameObject prefab;

    readonly List<ThumbnailVo> thumbnailVos = new();
    readonly List<Thumbnail> thumbnailPool = new();

    int Lines => Thumbnails / columns;
    int columns;
    float thumbnailHeight;
    float spacing;
    int viewportHeight;
    int linesInViewport;
    RectTransform rect;

    void Start()
    {
        CreateThumbnailVos();

        var layout = container.GetComponent<GridLayoutGroup>();
        thumbnailHeight = layout.cellSize.y + layout.spacing.y;
        spacing = layout.spacing.y;
        columns = layout.constraintCount;

        rect = container.GetComponent<RectTransform>();
        rect.GetComponent<ContentSizeFitter>().verticalFit = ContentSizeFitter.FitMode.Unconstrained;
        viewportHeight = ( int )transform.parent.GetComponent<CanvasScaler>().referenceResolution.y;
        linesInViewport = ( int )( viewportHeight / thumbnailHeight );
        rect.sizeDelta = new Vector2( 0, thumbnailHeight * ( Lines - linesInViewport ) + spacing * 2 );

        StartCoroutine( AdjustScrollView() );
    }

    void CreateThumbnailVos()
    {
        for ( var i = 0; i < Thumbnails; i++ )
        {
            var thumbnailVo = new ThumbnailVo
            {
                id = i.ToString()
            };
            thumbnailVos.Add( thumbnailVo );
        }
    }

    IEnumerator AdjustScrollView()
    {
        var offset = new Vector2( 1, -1 ) * thumbnailHeight / 2 + new Vector2( -spacing / 2, 0 );
        while ( true )
        {
            for ( var i = 0; i < AdjustFrequency; i++ )
            {
                yield return new WaitForEndOfFrame();
            }

            // Figure out which lines to show
            var lineStart = ( int )( Math.Max( 0, rect.anchoredPosition.y - (VisibleLines/2-linesInViewport)*thumbnailHeight ) / thumbnailHeight );

            // Show them
            for ( var i = lineStart; i < lineStart + VisibleLines; i++ )
            {
                for ( var j = 0; j < columns; j++ )
                {
                    var voIndex = i * columns + j;
                    if ( voIndex >= thumbnailVos.Count )
                    {
                        break;
                    }

                    var thumbnailIndex = ( i - lineStart ) * columns + j;
                    if ( thumbnailIndex < 0 )
                    {
                        break;
                    }

                    if ( thumbnailPool.Count <= thumbnailIndex ) // Create if needed
                    {
                        thumbnailPool.Add( Instantiate( prefab, container ).GetComponent<Thumbnail>() );
                    }

                    // Move/Set the right thumbnails
                    // TODO: Can be further improved with a stack to only move/set the ones that need to be moved/set
                    var position = new Vector2( j * thumbnailHeight, i * -thumbnailHeight ) + offset;
                    thumbnailPool[ thumbnailIndex ].ThumbnailVo = thumbnailVos[ voIndex ];
                    thumbnailPool[ thumbnailIndex ].rect.anchoredPosition = position;
                }
            }
        }
    }

    void OnDestroy()
    {
        StopAllCoroutines();
    }
}