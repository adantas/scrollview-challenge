﻿using UnityEngine;
using UnityEngine.UI;

public class Thumbnail : MonoBehaviour
{
    public RectTransform rect;

    ThumbnailVo thumbnailVo;

    // Caching sprite and Text too because GetComponents are actually very expensive and was happening
    // each time the Vo was being set
    Image image;
    Text text;

    public ThumbnailVo ThumbnailVo
    {
        set
        {
            thumbnailVo = value;
            image.sprite = SpriteManager.instance.GetSprite( thumbnailVo.id );
            text.text = thumbnailVo.id;
        }
    }

    void OnEnable()
    {
        image = GetComponent<Image>();
        text = GetComponentInChildren<Text>();
        rect = gameObject.GetComponent<RectTransform>();
    }

    public void OnClick()
    {
        Debug.Log( "Thumbnail clicked: " + thumbnailVo.id );
    }
}